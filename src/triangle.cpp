#include <triangle.hpp>



namespace ek
{
	Triangle::Triangle(float r, int x, int y, int k, float velocity) // Конструктор
	{
		m_k = k;
		m_r = r;
		m_x = x;
		m_y = y;
		m_velocity = velocity;
		m_shape = new sf::CircleShape(m_r, m_k);
		m_shape->setOrigin(m_r, m_r);
		m_shape->setFillColor(sf::Color::White);
		m_shape->setPosition(m_x, m_y);
	}
	Triangle::~Triangle() // Деструктор
	{
		delete m_shape;
	}

	sf::CircleShape* Triangle::Get() { return m_shape; }

	void Triangle::Move()
	{
		m_x += m_velocity;
		m_shape->setPosition(m_x, m_y);
	}
	
	void Triangle::SetX(int x)
	{
		m_x = x;
		m_shape->setPosition(m_x, m_y);
	}

	int Triangle::GetX() { return m_x; }

	int Triangle::GetR() { return m_r; }


}